import base64
import io
from http.server import BaseHTTPRequestHandler, HTTPServer

import simplejson as simplejson
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib.dates as mdates
from PIL import Image


def remaining_time(date):
    delta = date - dt.datetime.now().date()
    return delta.days

class handler(BaseHTTPRequestHandler):

    def do_POST(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.data_string = self.rfile.read(int(self.headers['Content-Length']))
        datas = simplejson.loads(self.data_string)
        datas = sorted(datas['data'], key=lambda data: dt.datetime.strptime(data["endCE"], '%d/%m/%Y').date())

        linewidth = 6

        plt.xlabel("Jours restants")
        plt.gca().xaxis.set_major_locator(mdates.DayLocator())
        plt.grid()

        for data in datas:
            # x = dt.datetime.strptime(data["endCE"], '%d/%m/%Y').date()
            x = remaining_time(dt.datetime.strptime(data["endCE"], '%d/%m/%Y').date())
            y = data["num"]
            pvri = data['pvri']
            line_color = 'g' if x > 0 else 'r'
            # si il reste plus de 15 jours on met à 14 que ça prenne pas trop de place sur le schéma
            if x > 13:
                plt.plot([0, 13], [y, y], line_color, linewidth=linewidth)
            else:
                plt.plot([0, x], [y, y], line_color, linewidth=linewidth)
                plt.plot([x, x + 0.05], [y, y], 'r', linewidth=linewidth * 2)
            # if there is pvri
            if pvri != '':
                remaining_time_pvri = remaining_time(dt.datetime.strptime(pvri, '%d/%m/%Y').date())
                plt.plot([remaining_time_pvri, remaining_time_pvri + 0.05], [y, y], color='orange',
                         linewidth=linewidth * 2)

        plt.ylim(-1, 10)
        plt.tight_layout()

        img_buf = io.BytesIO()
        plt.savefig(img_buf, format='png')
        plt.close()
        #im = Image.open(img_buf)
        #buffered = io.BytesIO()
        #im.save(buffered, format="PNG")
        img_str = base64.b64encode(img_buf.getvalue())
        img_buf.close()

        self.wfile.write(img_str)

if __name__ == '__main__':
    server = HTTPServer(('', 8080), handler)
    server.serve_forever()