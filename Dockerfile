FROM docker-io-nexus.uttnetgroup.net/python:3.6-alpine

WORKDIR /app

COPY . .

RUN apk add g++ jpeg-dev zlib-dev libjpeg make
RUN pip install matplotlib simplejson Pillow

EXPOSE 8080

CMD ["python3", "server.py"]
