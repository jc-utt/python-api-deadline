# This is a sample Python script.

# Press Maj+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import matplotlib.pyplot as plt
import datetime as dt
import matplotlib.dates as mdates


def remaining_time(date):
    delta = date - dt.datetime.now().date()
    return delta.days


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    raw = [{'num': '22009',
            'endCE': '20/07/2023',
            'pvri': ''},
           {'num': '22008',
            'endCE': '07/06/2023',
            'pvri': ''}]

    datas = sorted(raw, key=lambda data: dt.datetime.strptime(data["endCE"], '%d/%m/%Y').date())

    linewidth = 6

    plt.xlabel("Jours restants")
    plt.gca().xaxis.set_major_locator(mdates.DayLocator())
    plt.grid()

    for data in datas:
        # x = dt.datetime.strptime(data["endCE"], '%d/%m/%Y').date()
        x = remaining_time(dt.datetime.strptime(data["endCE"], '%d/%m/%Y').date())
        y = data["num"]
        pvri = data['pvri']
        line_color = 'g' if x > 0 else 'r'
        # si il reste plus de 15 jours on met à 14 que ça prenne pas trop de place sur le schéma
        if x > 13:
            plt.plot([0, 13], [y, y], line_color, linewidth=linewidth)
        else:
            plt.plot([0, x], [y, y], line_color, linewidth=linewidth)
            plt.plot([x, x + 0.05], [y, y], 'r', linewidth=linewidth * 2)
        # if there is pvri
        if pvri != '':
            remaining_time_pvri = remaining_time(dt.datetime.strptime(pvri, '%d/%m/%Y').date())
            plt.plot([remaining_time_pvri, remaining_time_pvri + 0.05], [y, y], color='orange',
                     linewidth=linewidth * 2)

    plt.ylim(-1, 10)
    plt.tight_layout()
    plt.show()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
